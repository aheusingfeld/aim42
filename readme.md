# aim42 - Software Architecture Improvement Method



[aim42](http://aim42.org) is an open-source project to collect practices, patterns and methods to:

* analyze issues, risks, problems, symptoms and technical debt within software (and their organisations)
* evaluate those issues, prioritize them
* find appropriate remedies and strategies to improve, optimize and cure software (and/or their organisations)

aim42 has been founded by [Gernot Starke](http://gernotstarke.de), is supported by [innoQ Deutschland GmbH](http://innoq.com) and is licenced under the
liberal http://creativecommons.org/licenses/by-sa/4.0[Creative Commons Sharealike licence].

Find more details on our [project homepage](http://aim42.org).


